﻿//Tictactoe ungraded lab
using System.Dynamic;

namespace TicTacToe
{
    public class TictactoeGrid
    {
        //creating property called grid that is a 3x3 char array
        private char[,] _grid = new char[3,3];

        //creating constructor
        public TictactoeGrid()
        {
            for(int i = 0; i < _grid.GetLength(0); i++) //checks the outer loop with .GetLength(0)
            {
                for(int j = 0; j < _grid.GetLength(1); j++) //checks the inner loop .GetLength(1)
                {
                    _grid[i,j] = ' '; //initializing the grid as empty
                }
            }
        }

        //method that places the current player's symbol on the grid
        public void PlaceCharacter(char playerInput, int row, int col, char[, ] _grid)
        {
            //checks if the player's input is x or o 
            //checks if the row and col are between a valid range
           if((playerInput != 'X' && playerInput != 'O') || (row < 0 || row > _grid.GetLength(0) || col < 0 || col > _grid.GetLength(1)))
           {
                Console.WriteLine("Players can only place X or O and the position must be in a valid range");
           }
            //if row and col are in a valid range, the player can inputt a symbol
           _grid[row, col] = playerInput;
        }

        //method that will print the tictactoe grid
        public void PrintGrid(char[,] _grid)
        {
            //will loop over the array and place an indicator for the space
            for(int i = 0; i < _grid.GetLength(0); i++)
            {
                for(int j = 0; j < _grid.GetLength(1); j++)
                {
                    Console.Write("_ ");
                }
                Console.WriteLine("");//after inner loop finishs printing, move to next row
            }
        }

        //method that check if there is a winning row
        public char CheckRows(char[, ] _grid)
        {
            for(int i = 0; i < _grid.GetLength(0); i++)
            {
                //checking if the first symbol at the start of the row is empty, it should skip to the next row 
                char firstSymbol = _grid[i, 0];
                if(firstSymbol == ' ')
                {
                    continue;// skips to the next iteration of the for loop
                }

                bool isSame = true;
                for(int j = 0; j < _grid.GetLength(1); j++)
                {
                    //first checks if the the first value of the row is empty(cannot win if it does not start with a symbol)
                    //second checks if the current symbol is not the same at the next column of the same row
                    if(firstSymbol == ' ' || firstSymbol != _grid[i, j])
                    {
                        isSame = false; //all the symbols in the row are not the same
                        break; //no need to continue checking becayse it won't be the same
                    }
                }

                if(isSame)
                {
                    return firstSymbol;
                }
            }
            //will return nothing because if there was nox winning row
            return ' ';
        }

        //method that checks if there is a winning column
        public char CheckColumns(char[, ] _grid)
        {
            for(int i = 0; i < _grid.GetLength(0); i++)
            {
                //this time we are checking the columns, so checking the start of each column
                char firstSymbol = _grid[0, i];
                if(firstSymbol == ' ')
                {
                    continue;
                }

                bool isSame = true;
                for(int j = 0; j < _grid.GetLength(1); j++)
                {
                    //checking for _grid[j,i] because we are checking for columns and not row
                    if(firstSymbol == ' ' || firstSymbol != _grid[j, i]) 
                    {
                        isSame = false;
                        break;
                    }
                }

                if(isSame)
                {
                    return firstSymbol;
                }
            }
            return ' ';
        }

        //methodt that will check if there is a winning diagonal
        // public char CheckDiagonals(char[, ] _grid)
        // {
        //     for(int i = 0; i < _grid.GetLength(0); i++)
        //     {
        //         char firstSymbol = _grid[i, 0];
        //         char lastSymbol = _grid[i, _grid.GetLength(0) -1]; //should be at the last position

        //         //you can't get a winning diagonal if the first or last values are empty
        //         if(firstSymbol == ' ' || lastSymbol == ' ')
        //         {
        //             break;
        //         }
        //         return ' ';
        // }


        //method that calls the methods to check if there is a winner
        public String CheckGrid(char[, ] _grid)
        {
            for(int i = 0; i < _grid.GetLength(0); i++)
            {
                for(int j = 0; j < _grid.GetLength(1); j++)
                {
                    if(XWins(_grid))
                    {
                        return "X";
                    }
                    else if(OWins(_grid))
                    {
                        return "O";
                    }
                }
            }
            //when all spaces are filled but there is no winner
            return "Tie";
        }

        //helper method that returns true if x wins
        public Boolean XWins(char[, ] _grid)
        {
            if(CheckRows(_grid) == 'X')
            {
                return true;
            }
            else{
                return false;
            }
        }

        //helper method that returns true if o wins
        public Boolean OWins(char[, ] _grid)
        {
            if(CheckRows(_grid) == 'O')
            {
                return true;
            }
            else{
                return false;
            }
        }

        //should implement one for diagonal

        //helper method to check if the grid area is still empty
        public Boolean IsAreaEmpty(int row, int col)
        {
            //checking the specific position in the array
            if(_grid[row, col] == ' ')
            {
                return true;
            }
            else{
                return false;
            }
        }

        //helper method to check if the player input is valide
        public Boolean isCharacterValid(char playerInput)
        {
            if(playerInput == 'X' || playerInput == 'O')
            {
                return true; 
            }
            else
            {
                return false;
            }
        }

        public class TicTacToeGame
        {
            public static void Main(String[] args)
            {
                //players
                char currentPlayer = 'X';
                int playerCounter = 0;

                //should add a while loop that lets the game continue
                //game should continue until the game call CheckGrid
                //making a tictactoeGrid object
                TictactoeGrid game = new TictactoeGrid();

                bool isGameOngoing = true;
                while(isGameOngoing)
                {
                    //updating player symbols
                    if(playerCounter % 2 == 0)
                    {
                        currentPlayer = 'X';
                    }
                    else
                    {
                        currentPlayer = 'O';
                    }

                    game.PrintGrid(game._grid); //constantly verify grid status
                    Console.WriteLine($"Player {currentPlayer} place your symbol: \nEnter Row:");
                    int row = Convert.ToInt32(Console.ReadLine());

                    Console.WriteLine("Enter column");
                    int col = Convert.ToInt32(Console.ReadLine());

                    game.PlaceCharacter(currentPlayer, row, col, game._grid); //player updates the board game

                    
                    


                    playerCounter++; //moves to the next player
                }
            }
        }
    }
}